<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizacion extends CI_Controller {

	public function generar_cotizacion(){
		
		//Obtenemos el ID del cliente de interes
		$Id_Cliente = $this->input->post('cliente');
		//Obtenemos los datos generales del cliente
		$this->load->model('clientes/busquedad_clientes_m','cliente');
		$cliente = $this->cliente->datos_cliente($Id_Cliente);

		//Obtenemos los datos de la cotizacion

		$productos = array();
		$detalle_arte = $this->input->post('detalle_arte');
		$cantidad_arte = $this->input->post('cantidad_arte');
		$precio_arte = $this->input->post('precio_arte');
		$sub_total_arte = $this->input->post('sub_total_arte');

		$detalle_planchas = $this->input->post('detalle_planchas');
		$cantidad_planchas = $this->input->post('cantidad_planchas');
		$precio_planchas = $this->input->post('precio_planchas');
		$sub_total_planchas = $this->input->post('sub_total_planchas');

		$detalle_negativo = $this->input->post('detalle_negativo');
		$ancho_negativo = $this->input->post('ancho_negativo');
		$alto_negativo = $this->input->post('alto_negativo');
		$cantidad_negativo = $this->input->post('cantidad_negativo');
		$precio_negativo = $this->input->post('precio_negativo');
		$sub_total_negativo = $this->input->post('sub_total_negativo');
		
		$detalle_otros = $this->input->post('detalle_otros');
		$monto_otros = $this->input->post('monto_otros');

		
		$productos = array();
		$arte = $this->arregloCotizacion($detalle_arte,$cantidad_arte,$precio_arte,$sub_total_arte,null,null);
		$planchas = $this->arregloCotizacion($detalle_planchas,$cantidad_planchas,$precio_planchas,$sub_total_planchas,null,null);
		$negativo = $this->arregloCotizacion($detalle_negativo,$cantidad_negativo,$precio_negativo,$sub_total_negativo,$alto_negativo,$ancho_negativo);
		$otro = $this->arregloCotizacion($detalle_otros,null,null,$monto_otros,null,null);
		$productos = array_merge($arte,$planchas,$negativo,$otro);

		
		$data = array(
			'Titulo_Pagina' => 'Ingreso de Pedidos',
			'Mensaje' => '',
			'Cliente' => $cliente,
			'Productos' => $productos
		);

		$html = $this->load->view('pdf/cotizacion_memory', $data, TRUE);
		// Cargamos la librería
		$this->load->library('pdfgenerator');
		// definamos un nombre para el archivo. No es necesario agregar la extension .pdf
		$filename = 'cotizacion';// generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
		$this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
	 //  echo json_encode($this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait'));
	}

	private function arregloCotizacion($detalle,$cantidad,$precio,$subtotal,$alto, $ancho){

		$productos = array();
		if (count($detalle)>0) {
			foreach ($detalle as $detalle_elemento => $elemento) {
				$cant = $cantidad != null ? $cantidad[$detalle_elemento] : '';
				$prec = $precio != null ? $precio[$detalle_elemento] : '';
				$sub = $subtotal != null ? $subtotal[$detalle_elemento] : '';
				$alt = $alto != null ? $alto[$detalle_elemento] : '';
				$anc = $alto != null ? $ancho[$detalle_elemento] : '';
				array_push($productos, array('detalle'=>$elemento,
											 'cantidad'=>$cant,
											 'precio'=>$prec,
											 'subtotal'=> $sub,
											 'alto'=> $alt,
											 'ancho'=> $anc
											)
				);
			}

		}
		return $productos;
	}
}
