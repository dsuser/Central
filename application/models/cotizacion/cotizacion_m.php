<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cotizacion_m extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	 *Agregar cotizaciones al sistema.
	 *Agregaremos los datos de cotizacion que sean requeridos en el sistema.
	 *@return string: 'ok' si el cliente se ingresa exitosamente
	 *@return string: 'error' si no se puede guardar el cliente.
	*/
	function ingresarCotizacion($id_pedido,$detalle_cotizacion,$ancho,$alto,$cantidad,$precio,$monto)
	{
		$datos_insertar = array(
   			'id_pedido' => $id_pedido,
   			'detalle_cotizacion' => $detalle_cotizacion,
   			'ancho' => $ancho,
   			'alto' => $alto,
   			'cantidad' => $cantidad,
   			'precio' => $precio,
   			'monto' => $monto,
		);
		$this->db->insert('cotizacion_pedido', $datos_insertar); 
		return 'ok';
	}
	
	
	
	

}

/* Fin del archivo */