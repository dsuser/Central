<!DOCTYPE html>
<html lang="en">
<head>
	<style>
	@page { margin: 180px 50px; }
	#header { position: fixed; left: 0px; top: -165px; right: 0px; height: 150px; padding-bottom: 130px }
	#footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px;}
	#footer .page:after { content: counter(page, upper-roman); }

	.table {
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		position: relative; 
		/*width: 700px; 
		height: 30px; 
		padding: 1px; */
		top: 70px; 
		bottom: 10px; 
		left: 10px; 
		z-index: 1;
	}

	.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
		position: relative;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}

	.col-lg-12 {
		width: 100%;
	}
	.table td, .table th {
		border: 1px solid #ddd;
		padding: 8px;
		font-size: xx-small;
	}

	.table tr:nth-child(even){background-color: #f2f2f2;}

	.table tr:hover {background-color: #ddd;}

	.table th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #EBC640;
		color: white;
		font-size: xx-small;
	}

	.table tr:nth-child(even){background-color: #f2f2f2;}

	.table tr:hover {background-color: #ddd;}

	.table th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #EBC640;
		color: white;
		font-size: xx-small;
	}
	body {
		font-family: Helvetica, Arial, sans-serif;
		font-size: 12px;
		line-height: 1.42857143;
		color: #333;
		background-color: #fff;
	}

	.text-center{
		text-align: center;
	}
	.linea{
		display: block;
		height: 1px;
		border: 0;
		border-top: 1px solid #38BCA0;
		margin: 1em 0;
		padding: 0;
	}
	.condiciones{
		/*position: relative; width: 100%; height: 30px; padding: 1px; 
		background-color: #0df;  right: 10px; z-index: 9; top: 70px; bottom: -500px;*/
		position: fixed; left: 0px; bottom: -30px; right: 0px; height: 150px;
	}
</style>

</head>
<body>
	<div id="header">
		<div class="col-md-6">
			<img src="<?php echo $_SERVER['DOCUMENT_ROOT']; ?>/html/img/lgtps/logo_pdf.png" alt="logo" width="250" height="75" >
		</div>
		<div class="col-md-6">
			<h4>Cotizacion #</h4> 
		</div>

		<hr class="linea" >
		<h4>Cliente: <?php if (isset($Cliente[0]['nombre'])): ?>
		<?=$Cliente[0]['nombre']?>
		<?php endif ?></h4>
		<h4>Nombre Proyecto: <?php if (isset($Cliente[0]['proceso'])): ?>
		<?=$Cliente[0]['proceso']?>
		<?php endif ?></h4>
		<h4>Atencion a:</h4>
	</div>
	<div id="footer">
		<hr class="linea">
		<p class="text-center">DIRECCION</p>
		<p class="text-center">Colonia San Benito, Calle La Reforma y Avenida Las palmas No 111, San Salvador, El Salvador</p> 
		<p class="text-center"><b>Teléfono Oficina: (503) 2223-5941; (503)2223-8035</b></p> 
		<p class="text-center"><a href="http://www.centralgraphics-cg.com">http://www.centralgraphics-cg.com</a></p> 
	</div> 
	<div id="content" >
	</div>

	<div >
		<table class="table">
			<thead>
				<tr>
					<th>ITEM</th>
					<th>DESCRIPCION</th>
					<th>LADO</th>
					<th>Precio Unitario</th>
					<th>Cantidad</th>
					<th>Medida en Ancho</th>
					<th>Medida en Alto</th>
					<th>Repeticiones</th>
					<th>Pulgadas cuadradas</th>
					<th>Subtotal</th>
				</tr>
			</thead>
			<tbody>
				<?php $contador = 1 ?>
				<?php foreach ($Productos as $producto => $data): ?>

					<tr>
						<td><?= $contador ?></td>
						<td><?= $data['detalle']?></td>
						<td></td>
						<td><?= $data['precio']?></td>
						<td><?= $data['cantidad']?></td>
						<td><?= $data['ancho']?><</td>
						<td><?= $data['alto']?><</td>
						<td></td>
						<td></td>
						<td><?= $data['subtotal']?></td>
					</tr>
					<?= $contador++ ?>
				<?php endforeach ?>
			</tbody>
		</table>

		<div class="condiciones">
			<p class="text-center">1. El tiempo de entrega será de 3 días, después de recibida la orden de compra.</p>
			<p class="text-center">2. Las emergencias pueden atenderse en un plazo menor, que será negociado e <br> informado cuando
						   se realice la consulta. Los cargos adicionales se originan <br> en los casos puntuales que se incurran
						   en horas extras</p> 
			<p class="text-center">3. La factura entregada es exportación, se facturará en dólares desde nuestra <br> oficina en El Salvador.
						   El flete hasta las oficinas del <br> transportista está incluido.</p>
		</div>
	</body>
	</html>

