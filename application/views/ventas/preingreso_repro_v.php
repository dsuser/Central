<script type="text/javascript" src="/html/js/pedido.003.js?n=1"></script>
<script type="text/javascript" src="/html/js/procesos.js?n=1"></script>
<script src="/html/js/jquery.min.js"></script>
<script src="/html/js/jquery-ui.min.js"></script>
<script src="/html/js/bootstrap.min.js"></script>
<script src="/html/js/wizard.min.js"></script>
<link href="/html/css/wizard.css" rel="stylesheet">
<script type="text/javascript" src="/html/js/acciones.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/html/css/dropify.min.css">

<script src="/html/js/cbpFWTabs.js"></script>
<script src="/html/js/dropify.js"></script>
<link href="/html/css/style.css" rel="stylesheet">
<!-- <link href="/html/css/tab_vertical.css" rel="stylesheet"> -->
<style>
    .desactivado {
        pointer-events: none;
    }
    
    .dropify-wrapper {
        height: 114px;
    }
</style>

<?php
$Icono_Ruta = array(
    2 => 'fdi_pdf',
    5 => 'fdi_liberacion',
    28 => 'fdi_aprobacion',
    9 => 'fpr_cilindro',
    10 => 'fpr_pt'
);
?>

    <form name="form_espec_repro" id="form_espec_repro" action="/ventas/preingreso/ingresar" method="post" enctype="multipart/form-data">
        <input type="hidden" id="cliente" name="cliente" value="" />
        <div class="panel-group wiz-aco" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Datos Generales
                    </a>
                </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <div class="container-fluid">
                            <!-- ***************** -->
                            <table class="table table-condensed table-borderless">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="">C&oacute;d. Cliente</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control input-sm" type="text" name="codigo_cliente" id="codigo_cliente" value="" onchange="vercliente(this.value)" size="10" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="">Proceso:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="proceso" class="form-control input-sm" id="proceso" value="" onblur="verifica_proceso()" />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="button" onclick="genera_correlativo()" value="Generar" class="btn btn-info btn-lg" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="">Cliente:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control input-sm" type="text" name="nombre_cliente" id="nombre_cliente" value="" disabled="disabled" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Producto:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control input-sm" type="text" name="producto" id="producto" value="" size="50" />
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Fecha Ingreso:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <?=date('d-m-Y')?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Fecha de Entrega:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-control input-sm" type="text" readonly="readonly" name="fecha_entrega" id="fecha_entrega" size="12" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <label for="">Miniatura:</label>
                                        </div>
                                        <div class="row">
                                            <div class="pull-left col-md-12">
                                                <input type="file" id="scan_pedido" name="scan_pedido" class="dropify" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <tr>
                                    <td></td>
                                    <!-- <td> <a  href="javascript:ver_agregar_scan('800-imagen_proceso');" class="iconos iscan toolizq"><span>Agregar Miniatura</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar&nbsp;Miniatura.</a></td>
                                -->
                                    <!-- <td> <button type="button" data-toggle="modal" data-target="#exampleModal" class="iconos iscan toolizq"><span>Agregar Miniatura</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar&nbsp;Miniatura.</button></td> -->

                                    <!--  <td>
                                    <div class="col-sm-6 ol-md-6 col-xs-12">
                                        <div class="white-box">
                                            <input type="file" id="input-file-now" class="dropify btn-link"/> 
                                        </div>
                                    </div>                                  
                                </td>        -->

                                </tr>
                            </table>
                        </div>
                        <div class="ui-widget" id="nuevo_proc" style="display: none;">
                            <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
                                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> Este proceso es nuevo.
                                    <br />Favor digitar Nombre del Producto.</p>
                            </div>
                        </div>

                        <div class="ui-widget" id="proceso_proc" style="display: none;">
                            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> No se puede Crear un Pedido Nuevo.
                                    <br />Este Proceso tiene una Ruta sin finalizar.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Especificaciones
                            </a>
                        </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <!-- ***************** -->
                        <strong>Tipo de trabajo:</strong>
                        <select name="tipo_trabajo">
                            <?php
                                foreach($Tipos_Trabajo as $Tipo)
                                {
                                    ?>
                                <option value="<?=$Tipo['id_tipo_trabajo']?>">
                                    <?=$Tipo['trabajo']?>
                                </option>
                                <?php
                                }
                                ?>
                        </select>
                        <br />
                        <br />

                        <!-- ***************** -->
                        <strong>Tipo de Impresi&oacute;n:</strong>
                        <?php
                            foreach($Tipos_Impresion as $Tipo)
                            {
                                ?>
                            <input type="radio" name="id_tipo_impresion" id="iti_<?=$Tipo['id_tipo_impresion']?>" value="<?=$Tipo['id_tipo_impresion']?>" <?php if($Tipo[ 'id_tipo_impresion']==2 ) { ?> checked="checked"
                            <?php
                                }
                                ?>
                                />
                                <label for="iti_<?=$Tipo['id_tipo_impresion']?>">
                                    <?=$Tipo['tipo_impresion']?>
                                </label>
                                <?php
                            }
                            ?>

                                    <!-- ***************** -->
                                    <strong>Embobinado</strong>

                                    <br />
                                    <br /> Cara:
                                    <?php
                            $Embobinado = 1;
                            for($i = 1; $i <= 8; $i++)
                            {
                                ?>
                                        &nbsp;
                                        <input type="radio" name="embobinado_cara" id="embo_cara_<?=$i?>" value="<?=$i?>" <?=($i==$Embobinado)? ' checked="checked"': ''?> />
                                        <label for="embo_cara_<?=$i?>"><span class="embobinados embo<?=$i?>"></span></label>
                                        <?php
                            }
                            ?>

                                            <br />
                                            <br /> Dorso:
                                            <?php
                            for($i = 1; $i <= 8; $i++)
                            {
                                ?>
                                                &nbsp;
                                                <input type="radio" name="embobinado_dorso" id="embo_dorso<?=$i?>" value="<?=$i?>" <?=($i==$Embobinado)? ' checked="checked"': ''?> />
                                                <label for="embo_dorso<?=$i?>"><span class="embobinados embo<?=$i?>"></span></label>
                                                <?php
                            }
                            ?>
                                                    <!-- ***************** -->
                                                    <br>
                                                    <br>

                                                    <!-- ***************** -->
                                                    <strong>Material Solicitado</strong>

                                                    <?php
                            foreach($Mat_Solicitado as $Material)
                            {
                                if('&nbsp;' == $Material['material_solicitado'])
                                {
                                    continue;
                                }
                                ?>
                                                        &nbsp; &nbsp;
                                                        <input type="checkbox" name="mat_solicitado_<?=$Material['id_material_solicitado_grupo']?>" id="mat_solicitado_<?=$Material['id_material_solicitado_grupo']?>" <?php if(isset($Especs[ 'matsolgru'][$Material[ 'id_material_solicitado_grupo']])) { ?> checked="checked"
                                                        <?php
                                }
                                ?> />
                                                            <label for="mat_solicitado_<?=$Material['id_material_solicitado_grupo']?>">
                                                                <?=$Material['material_solicitado']?>
                                                            </label>
                                                            <?php
                            }
                            ?>

                                                                <!-- <input type="checkbox" name="chk_impresion_digital" id="chk_impresion_digital" class="mues_ocul_dp" />
                            <label for="chk_impresion_digital"><strong>Impresi&oacute;n Digital</strong></label>

                            <div id="divchk_impresion_digital" style="display: none;">

                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 33%;" rowspan="2">

                                            <strong>Acabados</strong>

                                            <?php
                                            foreach($Tipo_Acabado as $Tipo)
                                            {
                                                ?>
                                                <br />
                                                <input type="checkbox" name="impd_acabado_<?=$Tipo['id_tipo_impd_acabado']?>" id="impd_acabado_<?=$Tipo['id_tipo_impd_acabado']?>" />
                                                <label for="impd_acabado_<?=$Tipo['id_tipo_impd_acabado']?>"><?=$Tipo['tipo_impd_acabado']?></label>
                                                <?php
                                            }
                                            ?>

                                        </td>

                                        <td style="width: 33%;">

                                            <strong>Material</strong>

                                            <?php
                                            foreach($Tipo_Material as $Tipo)
                                            {
                                                ?>
                                                <br />
                                                <input type="radio" name="id_tipo_impd_material" id="impd_material_<?=$Tipo['id_tipo_impd_material']?>" value="<?=$Tipo['id_tipo_impd_material']?>" />
                                                <label for="impd_material_<?=$Tipo['id_tipo_impd_material']?>"><?=$Tipo['tipo_impd_material']?></label>
                                                <?php
                                            }
                                            ?>
                                            <br />
                                            <input type="radio" name="id_tipo_impd_material" id="impd_material_100" value="100" />
                                            <label for="impd_material_100">Otro</label>
                                            <input type="text" name="otro_impd_material" id="otro_impd_material" value="" />

                                            <br /><br />
                                            <strong>Cant. Impresiones:</strong>
                                            <input type="text" name="cant_impresiones" id="cant_impresiones" value="" />

                                        </td>

                                        <td>

                                            <strong>Imposici&oacute;n</strong>

                                            <br />
                                            <input type="radio" name="imposicion" id="imposicion_t" value="t" />
                                            <label for="imposicion_t">Tiro</label>

                                            <br />
                                            <input type="radio" name="imposicion" id="imposicion_tr" value="tr" />
                                            <label for="imposicion_tr">Tiro y Retiro</label>

                                            <br /><br />
                                            <strong>Colores</strong>

                                            <br />
                                            Tiro:
                                            &nbsp; &nbsp; &nbsp;
                                            <input type="radio" name="tiro_color" id="tiro_color1" value="B/W" />
                                            <label for="tiro_color1">B/W</label>
                                            &nbsp; <input type="radio" name="tiro_color" id="tiro_color2" value="FC" />
                                            <label for="tiro_color2">FC</label>

                                            <br />
                                            Retiro: &nbsp;
                                            <input type="radio" name="retiro_color" id="retiro_color1" value="B/W" />
                                            <label for="retiro_color1">B/W</label>
                                            &nbsp; <input type="radio" name="retiro_color" id="retiro_color2" value="FC" />
                                            <label for="retiro_color2">FC</label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                            <strong>Otros Acabados:</strong>
                                            <input type="text" name="otro_acabado_imp" id="otro_acabado_imp" size="40" value="" />
                                        </td>
                                    </tr>
                                </table>
                            </div> -->

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Datos del Arte
                            </a>
                        </h4> </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <!-- ***************** -->
                        <table class="table table-borderless table-condensed">
                            <tr>
                                <!-- ***************** -->
                                <td style="width: 33%;">
                                    Unidad de Medida:
                                    <select name="unidad_medida" id="unidad_medida">
                                        <option value="mm">mm</option>
                                        <option value="in">in</option>
                                    </select>

                                    <br />
                                    <input type="text" size="9" name="alto_arte" id="alto_arte" value="" onblur="calcularPA()" /> Alto del Arte

                                    <br />
                                    <input type="text" size="9" name="ancho_arte" id="ancho_arte" value="" /> Ancho del Arte

                                    <br />
                                    <input type="text" size="9" name="ancho_fotocelda" id="ancho_fotocelda" value="" /> Ancho de Fotocelda

                                    <br />
                                    <input type="text" size="9" name="alto_fotocelda" id="alto_fotocelda" value="" /> Alto de Fotocelda

                                    <br />
                                    <input type="text" size="9" name="color_fotocelda" id="color_fotocelda" value="" /> Color Fotocelda

                                </td>

                                <!-- ***************** -->
                                <td style="width: 33%;">

                                    <strong>Impresi&oacute;n</strong>

                                    <br />
                                    <input type="radio" name="lado_impresion" id="lado_impresion_cara" value="cara" />
                                    <label for="lado_impresion_cara">Cara</label>

                                    &nbsp;

                                    <input type="radio" name="lado_impresion" id="lado_impresion_dorso" value="dorso" />
                                    <label for="lado_impresion_dorso">Dorso</label>

                                    <br />
                                    <br />

                                    <strong>Emulsi&oacute;n del Negativo</strong>

                                    <br />
                                    <input type="radio" name="emulsion_negativo" id="emulsion_negativo_cara" value="cara" />
                                    <label for="emulsion_negativo_cara">Cara</label>

                                    &nbsp;

                                    <input type="radio" name="emulsion_negativo" id="emulsion_negativo_dorso" value="dorso" />
                                    <label for="emulsion_negativo_dorso">Dorso</label>

                                </td>

                                <!-- ***************** -->
                                <td>

                                    <input type="checkbox" name="chk_impresion_cbd" id="chk_impresion_cbd" class="mues_ocul_dp" />
                                    <label for="chk_impresion_cbd"><strong>C&oacute;digo de Barra</strong></label>

                                    <div id="divchk_impresion_cbd" style="display: none;">
                                        <input type="text" size="19" name="codb_tipo" id="codb_tipo" value="" /> Tipo

                                        <br />
                                        <input type="text" size="19" name="codb_num" id="codb_num" value="" /> N&uacute;mero

                                        <br />
                                        <input type="text" size="19" name="codb_magni" id="codb_magni" value="" /> Magnificaci&oacute;n

                                        <br />
                                        <input type="text" size="19" name="codb_posicion" id="codb_posicion" value="" /> Posici&oacute;n

                                        <br />
                                        <input type="text" size="19" name="codb_bwr" id="codb_bwr" value="" /> BWR
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Datos de Montaje
                                </a>
                            </h4> </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <strong>Datos de Montaje</strong>

                        <table class="table table-bordered table-borderless">
                            <tr>
                                <td>
                                    <input type="text" size="9" name="repet_ancho" id="repet_ancho" value="1" />
                                </td>
                                <td>Repeticiones en Ancho</td>
                                <td>
                                    <input type="text" size="9" name="repet_alto" id="repet_alto" value="1" onblur="calcularPA()" />
                                </td>
                                <td>Repeticiones en Alto</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" size="9" name="separ_ancho" id="separ_ancho" value="0" />
                                </td>
                                <td>Separaci&oacute;n en Ancho</td>
                                <td>
                                    <input type="text" size="9" name="separ_alto" id="separ_alto" value="0" />
                                </td>
                                <td>Separaci&oacute;n en Alto</td>
                            </tr>
                        </table>

                        <!-- ***************** -->
                        <table>
                            <tr>
                                <!-- ***************** -->
                                <td>

                                    <table class="padding1">
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Colores</th>
                                            <th><span class="toolizq punteado">S<span>Color Solicitado</span></span>
                                            </th>
                                            <th><span class="toolizq punteado">T<span>Tono</span></span>
                                            </th>
                                            <th><span class="toolizq punteado">L<span>L&iacute;nea</span></span>
                                            </th>
                                            <th>&Aacute;ngulo</th>
                                            <th>Lineaje</th>
                                            <!-- th>Resoluci&oacute;n</th-->
                                        </tr>
                                        <?php
                                                    for($i = 1; $i <= 10; $i++)
                                                    {
                                                        ?>
                                            <tr>
                                                <td>
                                                    <?=$i?>
                                                </td>
                                                <td>
                                                    <input type="text" size="10" name="color_<?=$i?>" id="color_<?=$i?>" value="" onblur="poner_angulos('<?=$i?>')" />
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="solicitado_<?=$i?>" id="solicitado_<?=$i?>" onclick="pintar_caja('<?=$i?>')" />
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="tono_<?=$i?>" id="tono_<?=$i?>" />
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="linea_<?=$i?>" id="linea_<?=$i?>" />
                                                </td>
                                                <td>
                                                    <input type="text" size="4" name="angulo_<?=$i?>" id="angulo_<?=$i?>" value="" />
                                                </td>
                                                <td>
                                                    <input type="text" size="4" name="lineaje_<?=$i?>" id="lineaje_<?=$i?>" value="" />
                                                </td>
                                                <!-- td><input type="text" size="4" name="resolucion_<?=$i?>" id="resolucion_<?=$i?>" value="" /></td-->
                                            </tr>
                                            <?php
                                                    }
                                                    ?>
                                    </table>

                                </td>

                                <!-- ***************** -->
                                <td>

                                    <input type="checkbox" name="chk_guia_mont" id="chk_guia_mont" class="mues_ocul_dp" />
                                    <label for="chk_guia_mont"><strong>Gu&iacute;as de Montaje</strong></label>

                                    <table id="divchk_guia_mont" style="display: none;">
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="registro" id="registro" />
                                                <label for="registro">Gu&iacute;as de Registro</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="espectofotometria" id="espectofotometria" />
                                                <label for="espectofotometria">Espectofotometr&iacute;a</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="corte" id="corte" />
                                                <label for="corte">Gu&iacute;as de Corte</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="micropuntos" id="micropuntos" />
                                                <label for="micropuntos">Micropuntos</label>
                                            </td>
                                            <!-- <td colspan="2">
<input type="checkbox" name="semaforo" id="semaforo" />
<label for="semaforo">Sem&aacute;foros</label>
</td> -->
                                        </tr>
                                        <!-- <tr>
<td>
<input type="checkbox" name="grafikontrol" id="grafikontrol" />
<label for="grafikontrol">Grafikontrol</label>
</td>
<td colspan="2">
<input type="text" name="color" id="color" value="" />
Color
</td>
</tr> -->
                                    </table>

                                    <br />

                                    <input type="checkbox" name="chk_distorsion" id="chk_distorsion" class="mues_ocul_dp" />
                                    <label for="chk_distorsion"><strong>Distorsi&oacute;n</strong></label>

                                    <table id="divchk_distorsion" style="display: none;">
                                        <tr>
                                            <th colspan="3">
                                                <a href="javascript:limpiar()">[Borrar Datos]</a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="radio" id="radio" size="6" value="" onblur="calcular_distorsion()" /> Radio
                                            </td>
                                            <td>
                                                <?php

            $Stickyback = array('0.015', '0.020', '0.0177', '0.250', '0.60');
            ?>
                                                    <select name="polimero" id="polimero" onchange="calcular_distorsion()">
                                                        <option value="">Polimero</option>
                                                        <?php
                foreach($Polimeros as $polimero)
                {
                    ?>
                                                            <!-- <option value="<?=$polimero['clave_polimero']."-".$polimero['valor_polimero']?>"><?=$polimero['clave_polimero']?></option> -->
                                                            <option value="<?=$polimero['clave_polimero']?>">
                                                                <?=$polimero['clave_polimero']?>
                                                            </option>
                                                            <?php
                }
                ?>
                                                    </select>
                                            </td>
                                            <td>
                                                <select name="stickyback" id="stickyback" onchange="calcular_distorsion()">
                                                    <option value="">StickyBack</option>
                                                    <?php
                foreach($Stickyback as $index => $Valor)
                {
                    ?>
                                                        <option value="<?=$Valor?>">
                                                            <?=$Valor?>
                                                        </option>
                                                        <?php
                }
                ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="k" id="k" size="6" value="" />
                                                <span data-toggle="tooltip" data-placement="top" title="Constante (Valor autom&aacute;tico)" class="toolder punteado" class="toolder punteado">(K)<span>Constante (Valor autom&aacute;tico)</span></span>
                                            </td>
                                            <td>
                                                <input type="text" name="pb" id="pb" size="6" value="" onblur="calcular_distorsion()" />
                                                <span data-toggle="tooltip" data-placement="top" title="Per&iacute;metro Base" class="toolder punteado">PB<span>Per&iacute;metro Base</span></span>
                                            </td>
                                            <td>
                                                <input type="text" name="pa" id="pa" size="6" value="" onblur="calcular_distorsion()" />
                                                <span data-toggle="tooltip" data-placement="top" title="Per&iacute;metro Aumentado" class="toolder punteado">PA<span>Per&iacute;metro Aumentado</span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="dp" id="dp" size="6" value="" />
                                                <span data-toggle="tooltip" data-placement="top" title="Distorsi&oacute;n Positiva" class="toolder punteado">DP<span>Distorsi&oacute;n Positiva</span></span>
                                            </td>
                                            <td>
                                                <input type="text" name="dn" id="dn" size="6" value="" />
                                                <span data-toggle="tooltip" data-placement="top" title="Distorsi&oacute;n Negativa" class="toolder punteado">DN<span>Distorsi&oacute;n Negativa</span></span>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                Aplicar Ruta
            </a>
        </h4> </div>
                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Aplicar Ruta:
                        <select id="asigna_ruta" name="asigna_ruta">
                            <option value="">Seleccione</option>
                            <?php
                    foreach($Detalle_Rutas as $Id_Ruta => $Rutina)
                    {
                        ?>
                                <option value="<?=$Id_Ruta?>">
                                    <?=$Rutina['elemento']?>
                                </option>
                                <?php
                    }
                    ?>
                        </select>

                        <br />
                        <div id="ruta_trabajo">
                            <?php
//Quien tiene asignado el trabajo actualmente?
                    $Puesto_Asignado = 0;
//Puestos originales, para comparar si se realizaron cambios
                    $Puestos_Originales = array();
//La fecha de entrega se calcula para los pedidos que se agregaran con ruta cero,
//si es modificar ruta, no se calcula entrega
                    $Agrega_Modifica = 'agrega';

                    if(0 == count($Ruta_Actual))
                    {
//echo 'Es ingreso nuevo';
                    }

                    ?>
                        </div>

                        <div id="ruta_trabajos"></div>

                        <br />
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Cotizacion
                </a>
            </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <!-- INICIO TABLAS -->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="white-box">

                                <div class="vtabs">
                                    <ul class="nav tabs-vertical">
                                        <li role="presentation" class="active"><a href="#arte" data-toggle="tab"><span><i class=" material-icons"> color_lens</i> ARTE</span> </a></li>
                                        <li role="presentation"><a href="#negativos" data-toggle="tab"><span><i class=" material-icons"> theaters</i> NEGATIVOS</span></a></li>
                                        <li role="presentation"><a href="#plancha_fotopolimeras" data-toggle="tab"><span><i class=" material-icons"> crop_portrait</i> PLANCHAS FOTOPOLIMERAS</span></a></li>
                                        <!-- <li role="presentation"><a href="#prueba_color" data-toggle="tab"><span><i class=" material-icons"> hdr_strong</i> PRUEBA DE COLOR</span></a></li> -->
                                        <li role="presentation"><a href="#otros" data-toggle="tab"><span><i class=" material-icons"> autorenew</i> OTROS</span> </a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="arte">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <button id="btn_add" class="btn btn-xs" info="arte" type="button" data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila"><i class="material-icons">add</i></button>
                                                </div>
                                                <div class="col-md-11 pull-right">
                                                    <table id="tabla_arte" class="table table-bordered table-responsive table-hover">
                                                        <thead class="text-primary">
                                                            <tr>
                                                                <th>DETALLE ARTE</th>
                                                                <th>CANTIDAD</th>
                                                                <th>PRECIO</th>
                                                                <th>SUB-TOTAL</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="body_arte">

                                                        </tbody>
                                                        <tfoot  style="font-weight:bold;">
                                                            <tr>
                                                                <td>TOTAL</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td id="total_arte"></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="negativos">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <button id="btn_add" class="btn btn-xs" info="negativo" type="button" data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila"><i class="material-icons">add</i></button>

                                                    </div>
                                                    <div class="col-md-11">
                                                        <table id="tabla_negativo" class="table table-bordered table-responsive table-hover table-condensed">
                                                            <thead class="text-primary">
                                                                <tr>
                                                                    <th>DETALLE MONTAJE</th>
                                                                    <th>ANCHO</th>
                                                                    <th>ALTO</th>
                                                                    <th>CANTIDAD</th>
                                                                    <th>PRECIO</th>
                                                                    <th>SUB-TOTAL</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="body_negatico">

                                                            </tbody>
                                                            <tfoot style="font-weight:bold;">
                                                                <tr>
                                                                    <td>TOTAL</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td id="total_negativo"></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="plancha_fotopolimeras">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <button id="btn_add" class="btn btn-xs" info="planchas" type="button" data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila"><i class="material-icons">add</i></button>

                                                </div>
                                                <div class="col-md-11">
                                                    <table id="tabla_planchas" class="table table-bordered table-responsive table-hover">
                                                        <thead class="text-primary">
                                                            <tr>
                                                                <th>DETALLE</th>
                                                                <th>CANTIDAD</th>
                                                                <th>PRECIO</th>
                                                                <th>SUB-TOTAL</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="body_planchas">

                                                        </tbody>
                                                        <tfoot style="font-weight:bold;">
                                                            <tr>
                                                                <td>TOTAL</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td id="total_planchas" ></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="prueba_color">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <button id="btn_add" class="btn btn-xs" info="color" type="button" data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila"><i class="material-icons">add</i></button>

                                                </div>
                                                <div class="col-md-11">
                                                    <table id="tabla_color" class="table table-bordered table-responsive table-hover">
                                                        <thead class="text-primary">
                                                            <tr>
                                                                <th>DETALLE ARTE</th>
                                                                <th>CANTIDAD</th>
                                                                <th>PRECIO</th>
                                                                <th>SUB-TOTAL</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                        <tfoot style="font-weight:bold;">
                                                            <tr>
                                                                <td>TOTAL</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>$ 0:00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="otros">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <button id="btn_add" class="btn btn-xs" info="otros" type="button" data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila"><i class="material-icons">add</i></button>

                                                </div>
                                                <div class="col-md-11">
                                                    <table id="tabla_otros" class="table table-bordered table-responsive table-hover">
                                                        <thead class="text-primary">
                                                            <tr>
                                                                <th style="width: 175px;">DETALLE</th>
                                                                <th style="width: 125px;">MONTO</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="body_otros">

                                                        </tbody>
                                                        <tfoot style="font-weight:bold;">
                                                            <tr>
                                                                <td>TOTAL</td>
                                                                <td id="total_otros"></td>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="text-center"> <button type="button" id="cotizacionPDF" class="btn btn-info btn-sm">Generar Cotizacion</button>
                                </div>
                            </div>
                        </div>
                        <!-- FIN TABLAS -->
                        <input type="checkbox" name="cotizacion" id="cotizacion" style="display: none;" checked="checked" />
                        <div id="div_cotizaciones"></div>

                        <br />
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                Observaciones
            </a>
        </h4> </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <strong>Observaciones</strong>
                        <br />
                        <textarea name="observaciones" id="observaciones" rows="4" cols="60"></textarea>

                    </div>
                </div>
            </div>
        </div>

        <!-- <input type="button"  data-toggle="tooltip" data-placement="top" title="Agregar Nueva Fila" value="Agregar Pre-Ingreso" id="agr_pre_ingreso" disabled="disabled" onclick="guardar_preingreso()" /> -->
    </form>
    <!-- Area del modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Miniatura</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="white-box">
                        <h3 class="box-title"></h3>
                        <label for="input-file-now-custom-1">Seleccione Una Imagen</label>
                        <input type="file" id="input-file-now-custom-1" class="dropify" data-default-file="../plugins/bower_components/dropify/src/images/test-image-1.jpg" />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin area Modal -->
    <style type="text/css">
        .ta100td25 {
            width: 100%;
        }
        
        .ta100td25 td {
            width: 25%;
        }
        
        #nuevo_proc,
        #proceso_proc {
            position: absolute;
            top: 200px;
            margin-left: 500px;
        }
    </style>

    <script type="text/javascript">
        $('#codigo_cliente').focus();

        $(function() {
            $("[name=fecha_entrega]").datepicker({
                dateFormat: 'dd-mm-yy',
                showButtonPanel: true
            });

            $('.mues_ocul_dp').click(function() {

                if ($('#' + $(this).attr('id')).is(':checked')) {
                    $('#div' + $(this).attr('id')).show();
                } else {
                    $('#div' + $(this).attr('id')).hide();
                }
            });

            $("#dialog").dialog({
                autoOpen: false,
                width: 460,
                resizable: false,
                modal: true
            });
        });

        poner_angulos('');

        //Pintamiento de cajas
        for (i = 1; i <= 10; i++) {
            pintar_caja(i);
        }

        function ver_preingreso_br() {
            if ('' == $('#producto').val() || '' == $('#nombre_cliente').val()) {
                alert('La informacion del Proceso no debe estar vacia');
                return false;
            }
            if ('none' == $('#nuevo_proc').css('display')) {
                guardar_preingreso()
            }
        }

        function enviar_espec() {
            if (confirm('La informaci\xf3n ser\xe1 modificada.\r\nDesea continuar?')) {
                $('#form_espec_repro').submit();
            }
        }
    </script>

    <!-- Scripts para el acordion -->
    <script type="text/javascript">
        (function() {
            $('#accordion').wizard({
                step: '[data-toggle="collapse"]',
                buttonsAppendTo: '.panel-collapse',
                buttonLabels: {
                    back: "Anterior",
                    next: "Siguiente",
                    finish: "Agregar Ingreso"
                },
                classes: {
                    step: {
                        done: 'done',
                        error: 'error',
                        active: 'current',
                        disabled: 'loading',
                        activing: 'activing',
                        loading: 'loading'
                    },

                    pane: {
                        active: 'active',
                        activing: 'activing',
                        disabled: 'disabled',
                    },

                    button: {
                        hide: 'hide',

                    }
                },
                enableWhenVisited: true,
                templates: {
                    buttons: function() {
                        var options = this.options;
                        return '<div class="panel-footer"><ul class="pager">' + '<li class="previous">' + '<a href="#' + this.id + '" data-wizard="back" role="button">' + options.buttonLabels.back + '</a>' + '</li>' + '<li class="next">' + '<a href="#' + this.id + '" data-wizard="next" role="button">' + options.buttonLabels.next + '</a>' + '<a href="#' + this.id + '" data-wizard="finish" role="button" onclick="guardar_preingreso()">' + options.buttonLabels.finish + '</a>' + '</li>' + '</ul></div>';
                    }
                },
                onBeforeShow: function(step) {
                    step.$pane.collapse('show');
                },
                onBeforeHide: function(step) {
                    step.$pane.collapse('hide');
                },
                onFinish: function() {
                    swal("Message Finish!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
                }
            });
        })();
    </script>

    <script>
        <?php
    $Usuarios_Array_v = array();
    foreach($Dpto_Usuario as $Index => $Fila)
    {
        $Usuarios_Array_v[$Index]['dpto'] = $Fila['dpto'];
        $Usuarios_Array_v[$Index]['tiempo'] = $Fila['tiempo'];
        $Usuarios_Array_v[$Index]['usuarios'] = array();
        foreach($Fila['usuarios'] as $Iusute => $Infosote)
        {
            unset($Infosote['activo'], $Infosote['programable'], $Infosote['usuario']);
            $Usuarios_Array_v[$Index]['usuarios'][$Iusute] = $Infosote;
        }
    }
    ?>
        var Icono_Ruta = <?=json_encode($Icono_Ruta, true)?>;
        var Dpto_Usuario = <?=json_encode($Usuarios_Array_v, true)?>;
        var Departamentos = <?=json_encode($Departamentos, true)?>;

        var Puestos_actuales = <?=count($Ruta_Actual)?>;

        <?php
    foreach($Puestos_Originales as $Id_Ruta_Dpto => $Id_Usu_Dpto)
    {
        ?>
        puestos_originales[<?=$Id_Ruta_Dpto?>] = <?=$Id_Usu_Dpto?>;
        <?php
    }
    ?>
        puesto_asignado = <?=$Puesto_Asignado?>;

        var Detalle_Rutas = {};
        <?php
    foreach($Detalle_Rutas as $Index => $Fila)
    {
        ?>
        Detalle_Rutas[<?=$Index?>] = <?=json_encode($Fila, true)?>;
        <?php
    }

    foreach($Ruta_Actual as $Index => $Fila)
    {
        if(
            is_numeric($Ruta_Actual[$Index]['tiempo_asignado'])
        )
        {
            $Ruta_Actual[$Index]['tiempo_asignado'] = $this->fechas_m->minutos_a_hora(
                $Ruta_Actual[$Index]['tiempo_asignado']
            );
        }
    }
    ?>
        var Ruta_Actual = <?=json_encode($Ruta_Actual, true)?>;
        var Nuevo_Viejo = "<?=(0==count($Ruta_Actual))?'nuevo':'viejo'?>";

        $('#asigna_ruta').change(function() {
            if (0 != $(this).val()) {

                $('#ruta_trabajo').empty();
                for (irup in Detalle_Rutas[$(this).val()].ruta) {

                    var Iddepito = Detalle_Rutas[$(this).val()].ruta[irup].id_dpto;
                    var Span_Seleccionado = ' flujo_rt_seleccionado';
                    var Chequesin = ' checked="checked"';
                    var val_hora = '0:00';
                    var Set_Usuario = '';
                    var Asignadote = '';

                    if ('viejo' == Nuevo_Viejo && 0 < <?=isset($Ruta_Aplicada)?$Ruta_Aplicada:'0'?>) {
                        if (undefined == Ruta_Actual[irup]) {
                            Span_Seleccionado = '';
                            Chequesin = '';
                        } else {
                            Set_Usuario = Ruta_Actual[irup]['id_usuario'];
                            val_hora = Ruta_Actual[irup]['tiempo_asignado'];
                            if (
                                'Agregado' != Ruta_Actual[irup]['estado'] && 'Terminado' != Ruta_Actual[irup]['estado']
                            ) {
                                Asignadote = ' checked="checked"';
                            }
                        }
                    }

                    var Puesto = '<span class="flujo_ruta_dpto fruta_' + irup + Span_Seleccionado + '">';
                    Puesto = Puesto + '<div class="flujo_div1 icono_ruta_puesto">';
                    Puesto = Puesto + '<span class="flicon ' + Icono_Ruta[Iddepito] + '" ruta="' + irup + '"></span>';
                    Puesto = Puesto + '</div>';
                    Puesto = Puesto + '<div class="flujo_div2">';
                    Puesto = Puesto + '<span class="toolizq">';
                    Puesto = Puesto + '<input type="radio" name="puesto_asignado" id="puas_' + irup + '" value="' + irup + '"' + Asignadote + ' />';
                    Puesto = Puesto + '<span>Asignar Pedido a <strong>' + Departamentos[Iddepito].departamento + '</strong></span>';
                    Puesto = Puesto + '</span>';
                    Puesto = Puesto + Departamentos[Iddepito].departamento;
                    Puesto = Puesto + '<br />';
                    Puesto = Puesto + '<input type="checkbox" name="chk_' + irup + '" id="chk_' + irup + '" info="' + irup + '"' + Chequesin + ' />';
                    Puesto = Puesto + '<select name="slc_' + irup + '" id="slc_' + irup + '">';

                    //Usuario/a que pertenecen al departamento en turno si es que hay usuarios
                    if (undefined != Dpto_Usuario[Iddepito].usuarios) {
                        for (iusu in Dpto_Usuario[Iddepito].usuarios) {
                            Puesto = Puesto + '<option value="' + iusu + '">' + Dpto_Usuario[Iddepito].usuarios[iusu].nombre + '</option>';
                        }
                    }

                    Puesto = Puesto + '</select>';

                    if ('s' == Departamentos[Iddepito].tiempo) {
                        Puesto = Puesto + '<input type="text" size="5" value="' + val_hora + '" name="tie_' + irup + '" id="tie_' + irup + '" onblur="validar_hora(\'tie_' + irup + '\')" />';
                    } else {
                        Puesto = Puesto + '<input type="hidden" value="N/A" name="tie_' + irup + '" id="tie_' + irup + '" />';
                    }

                    Puesto = Puesto + '</div>';
                    Puesto = Puesto + '</span>';

                    $('#ruta_trabajo').append(Puesto);

                    if ('' != Set_Usuario) {
                        $('#slc_' + irup).val(Set_Usuario);
                    }

                }

            }
        });

        $('.sele_puestos').click(function() {
            var chequear = false;
            if ('Todo' == $(this).text()) {
                chequear = true;
            }

            $('#ruta_trabajo input[type="checkbox"]').attr('checked', chequear).each(function() {
                var id_chk = $(this).attr('id');
                id_chk = id_chk.split('_');
                poner_color_fila('chk_' + id_chk[1], 'fila_' + id_chk[1]);
            });
        });

        $('#ruta_trabajo').on('click', '.flicon', function() {
            if ($('#chk_' + $(this).attr('ruta')).prop('checked')) {
                $(this).parent().parent().removeClass('flujo_rt_seleccionado');
            } else {
                $(this).parent().parent().addClass('flujo_rt_seleccionado');
            }

            $('#chk_' + $(this).attr('ruta')).click();
        });

        <?php
    if(isset($Ruta_Aplicada))
    {
        ?>
        $('#asigna_ruta').val(<?=$Ruta_Aplicada?>).change();
        <?php
    }
    ?>
    </script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function calcularPA() {
            ($('#alto_arte').val() != "") ? $('#pa').val($('#repet_alto').val() * $('#alto_arte').val()): $('#pa').val(0);
        }
    </script>

    <script>
        var index_otros = 0;
        var index_negativo = 0;
        var index_info = 0;
        var index_arte = 0;
        var index_plancha = 0;
        var index_color = 0;
        $('button#btn_add').click(function() {
            info = $(this).attr('info');
            switch (info) {
                case 'otros':
                    $('#tabla_' + info).append('<tr>' +
                        '<td>' + '<input type="text" class="detalle_otros_' + index_otros + ' form-control" name="detalle_otros[]"  style="width: 320px"/>' + '</td>' +
                        '<td>' + '<input onchange="calcularTotal(\'' + info + '_' + index_otros + '\')"  type="text" class="monto_otros_' + index_otros + ' form-control" name="monto_otros[]" style="width: 75px"/>' + '</td>' + '</tr>');
                    index_otros++;
                    break;
                case 'negativo':
                    $('#tabla_' + info).append('<tr>' +
                        '<td>' + '<input type="text" class="detalle_info_negativo_' + index_negativo + ' form-control" name="detalle_negativo[]" style="width: 175x"/>' + '</td>' +
                        '<td>' + '<input type="text" class="ancho_info_negativo_' + index_negativo + ' form-control" name="ancho_negativo[]" style="width: 65px"/>' + '</td>' +
                        '<td>' + '<input type="text" class="alto_info_negativo_' + index_negativo + ' form-control" name="alto_negativo[]" style="width: 65px"/>' + '</td>' +
                        '<td>' + '<input type="text" class="cantidad_info_negativo_' + index_negativo + ' form-control" name="cantidad_negativo[]" onchange="calcularSubtotal(\'' + info + '_' + index_negativo + '\')" style="width: 75px"/>' + '</td>' +
                        '<td>' + '<input type="text" class="precio_info_negativo_' + index_negativo + ' form-control" name="precio_negativo[]" onchange="calcularSubtotal(\'' + info + '_' + index_negativo + '\')" style="width: 65px"/>' + '</td>' +
                        '<td>' + '<input type="text" class="subtotal_info_negativo_' + index_negativo + ' form-control desactivado" name="sub_total_negativo[]" style="width: 75px" value="0"/>' + '</td>' + '</tr>');
                    index_negativo++;
                    break;
                default:
                    switch (info) {
                        case 'arte':
                            index_info = index_arte;
                            index_arte++;
                            break;
                        case 'planchas':
                            index_info = index_plancha;
                            index_plancha++;
                            break;
                        case 'color':
                            index_info = index_color;
                            index_color++;
                            break;
                    }
                    $('#tabla_' + info).append('<tr>' +
                        '<td>' + '<input type="text" class="detalle_info_' + info + '_' + index_info + ' form-control" name="detalle_' + info + '[]" style="width: 225px"/>' + '</td>' +
                        '<td>' + '<input onchange="calcularSubtotal(\'' + info + '_' + index_info + '\')" type="text" class="cantidad_info_' + info + '_' + index_info + ' form-control" name="cantidad_' + info + '[]" style="width: 75px"/>' + '</td>' +
                        '<td>' + '<input onchange="calcularSubtotal(\'' + info + '_' + index_info + '\')" type="text" class="precio_info_' + info + '_' + index_info + ' form-control" name="precio_' + info + '[]"  style="width: 75px"/>' + '</td>' +
                        '<td>' + '<input type="text" class="subtotal_info_' + info + '_' + index_info + ' form-control desactivado" name="sub_total_' + info + '[]" style="width: 75px" value="0"/>' + '</td>' +
                        +'</tr>');

                    break;
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        function calcularSubtotal(info_id) {
            //Aca se calcula el total por columna
            $('.subtotal_info_' + info_id).val(($('.cantidad_info_' + info_id).val().trim()) * ($('.precio_info_' + info_id).val().trim()));
            calcularTotal(info_id)
        }
    </script>

    <script>
        function calcularTotal(info_id) {
            //Aca se calcula el total por cada tabla
            
            var info = info_id.split("_");
            switch (info[0]) {
                case 'arte':
                    var total_tabla = 0;
                    $('#body_arte').find('tr').each(function () {
                        var row = $(this);
                        total_tabla = total_tabla + parseFloat(row.find('td').find('input').eq(3).val());
                    });
                    $('#total_arte').text('$ ' + total_tabla.toFixed(2));
                break;
                case 'negativo':
                    var total_tabla = 0;
                    $('#body_negatico').find('tr').each(function () {
                        var row = $(this);
                        total_tabla = total_tabla + parseFloat(row.find('td').find('input').eq(5).val());
                    });
                    $('#total_negativo').text('$ ' + total_tabla.toFixed(2));
                break;
                case 'planchas':
                    var total_tabla = 0;
                    $('#body_planchas').find('tr').each(function () {
                        var row = $(this);
                        total_tabla = total_tabla + parseFloat(row.find('td').find('input').eq(3).val());
                    });
                    $('#total_planchas').text('$ ' + total_tabla.toFixed(2));
                break;
                default:
                    var total_tabla = 0;
                    $('#body_otros').find('tr').each(function () {
                        var row = $(this);
                        total_tabla = total_tabla + parseFloat(row.find('td').find('input').eq(1).val());
                    });
                    $('#total_otros').text('$ ' + total_tabla.toFixed(2));
                break;
            }      
        }
    </script>

    <script>
        function buscar_cotizacion() {
            $.get("/productos/listar_productos/index/" + $("#cliente").val(), function(d) {
                // alert("/productos/listar_productos/index/" + $("#cliente").val());
                var productos = JSON.parse(d);
                info = ["arte", "negativo", "planchas", "otros"];
                var nueva_tabla_arte;
                var nueva_tabla_negativo;
                var nueva_tabla_plancha;
                var nueva_tabla_otro;


                for (var producto in productos['prod']) {
                    id = parseInt(productos['prod'][producto]['id']);
                    // Productos de Arte Activos
                    if (id == 5 || id == 7 || id == 8) {
                        nueva_tabla_arte += '<tr>';
                        nueva_tabla_arte += '<td>';
                        nueva_tabla_arte += '<input type="text" class="detalle_info_' + info[0] + '_' + productos['prod'][producto]['idc'] + ' form-control"  name="detalle_' + info[0] + '[]" value="' + productos['prod'][producto]['pro'] + '" style="width: 225px"/>';
                        nueva_tabla_arte += '</td>';
                        nueva_tabla_arte += '<td>';
                        nueva_tabla_arte += '<input onchange="calcularSubtotal(\'' + info[0] + '_' + productos['prod'][producto]['idc'] + '\')" value="' + productos['prod'][producto]['can'] + '" type="text" class="cantidad_info_' + info[0] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="cantidad_' + info[0] + '[]" style="width: 75px"/>';
                        nueva_tabla_arte += '</td>';
                        nueva_tabla_arte += '<td>';
                        nueva_tabla_arte += '<input onchange="calcularSubtotal(\'' + info[0] + '_' + productos['prod'][producto]['idc'] + '\')" value="" type="text" class="precio_info_' + info[0] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="precio_' + info[0] + '[]"  style="width: 75px"/>';
                        nueva_tabla_arte += '</td>';
                        nueva_tabla_arte += '<td>';
                        nueva_tabla_arte += '<input type="text" class="subtotal_info_' + info[0] + '_' + productos['prod'][producto]['idc'] + ' form-control desactivado" name="sub_total_' + info[0] + '[]" value="0" style="width: 75px"/>';
                        nueva_tabla_arte += '</td>';
                        nueva_tabla_arte += '</tr>';
                    }
                    // Productos de Negativo Activos
                    if (id == 34 || id == 39) {
                        nueva_tabla_negativo += '<tr>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input type="text" class="detalle_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control"  name="detalle_' + info[1] + '[]" value="' + productos['prod'][producto]['pro'] + '" style="width: 175px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input type="text" class="detalle_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="ancho_' + info[1] + '[]" value="" style="width: 65px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input type="text" class="detalle_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="alto_' + info[1] + '[]" value="" style="width: 65px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input onchange="calcularSubtotal(\'' + info[1] + '_' + productos['prod'][producto]['idc'] + '\')" value="' + productos['prod'][producto]['can'] + '" type="text" class="cantidad_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="cantidad_' + info[1] + '[]" style="width: 75px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input onchange="calcularSubtotal(\'' + info[1] + '_' + productos['prod'][producto]['idc'] + '\')" value="" type="text" class="precio_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="precio_' + info[1] + '[]"  style="width: 65px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '<td>';
                        nueva_tabla_negativo += '<input type="text" class="subtotal_info_' + info[1] + '_' + productos['prod'][producto]['idc'] + ' form-control desactivado" name="sub_total_' + info[1] + '[]" value="0" style="width: 75px"/>';
                        nueva_tabla_negativo += '</td>';
                        nueva_tabla_negativo += '</tr>';
                    }

                    // Productos de Planchas Activos
                    if (id == 18 || id == 29) {
                        nueva_tabla_plancha += '<tr>';
                        nueva_tabla_plancha += '<td>';
                        nueva_tabla_plancha += '<input type="text" class="detalle_info_' + info[2] + '_' + productos['prod'][producto]['idc'] + ' form-control"  name="detalle_' + info[2] + '[]" value="' + productos['prod'][producto]['pro'] + '" style="width: 225px"/>';
                        nueva_tabla_plancha += '</td>';
                        nueva_tabla_plancha += '<td>';
                        nueva_tabla_plancha += '<input onchange="calcularSubtotal(\'' + info[2] + '_' + productos['prod'][producto]['idc'] + '\')" value="' + productos['prod'][producto]['can'] + '" type="text" class="cantidad_info_' + info[2] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="cantidad_' + info[2] + '[]" style="width: 75px"/>';
                        nueva_tabla_plancha += '</td>';
                        nueva_tabla_plancha += '<td>';
                        nueva_tabla_plancha += '<input onchange="calcularSubtotal(\'' + info[2] + '_' + productos['prod'][producto]['idc'] + '\')" value="" type="text" class="precio_info_' + info[2] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="precio_' + info[2] + '[]"  style="width: 75px"/>';
                        nueva_tabla_plancha += '</td>';
                        nueva_tabla_plancha += '<td>';
                        nueva_tabla_plancha += '<input type="text" class="subtotal_info_' + info[2] + '_' + productos['prod'][producto]['idc'] + ' form-control desactivado" name="sub_total_' + info[2] + '[]" value="0" style="width: 75px"/>';
                        nueva_tabla_plancha += '</td>';
                        nueva_tabla_plancha += '</tr>';
                    }

                    // Productos de Otros Activos
                    if (id == 54 || id == 73 || id == 102 || id == 130 || id == 111) {
                        nueva_tabla_otro += '<tr>';
                        nueva_tabla_otro += '<td>';
                        nueva_tabla_otro += '<input type="text" class="detalle_info_' + info[3] + '_' + productos['prod'][producto]['idc'] + ' form-control"  name="detalle_' + info[3] + '[]" value="' + productos['prod'][producto]['pro'] + '" style="width: 320px"/>';
                        nueva_tabla_otro += '</td>';
                        nueva_tabla_otro += '<td>';
                        nueva_tabla_otro += '<input onchange="calcularTotal(\'' + info + '_' + index_otros + '\')" value="0" type="text" class="monto_info_' + info[3] + '_' + productos['prod'][producto]['idc'] + ' form-control" name="monto_' + info[3] + '[]" style="width: 75px"/>';
                        nueva_tabla_otro += '</td>';
                        nueva_tabla_otro += '</tr>';
                    }
                }

                $('#tabla_arte').append(nueva_tabla_arte);
                $('#tabla_negativo').append(nueva_tabla_negativo);
                $('#tabla_planchas').append(nueva_tabla_plancha);
                $('#tabla_otros').append(nueva_tabla_otro);
                index_arte = index_negativo = index_plancha = index_otros = parseInt(productos['prod'][producto]['idc']) + 1;
                //index_negativo = parseInt(productos['prod'][producto]['idc']) + 1;
            });
        }
    </script>

    <!-- funcion para eliminar archivo-->
    <script>
        $(document).ready(function() {

            // Translated
            $('.dropify').dropify({
                messages: {
                    default: 'Arrastre y Suelte un Archivo o Haga Click aquí',
                    replace: 'Arrastre y Suelte un Archivo para Reemplazar o haga Click aquí',
                    remove: 'Eliminar',
                    error: 'A Ocurrido un Error'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    <script>
        $('#cotizacionPDF').click(function(event) {
            var form = $('#form_espec_repro');
            var xhr = new XMLHttpRequest();
            var url = '/pdf/cotizacion/generar_cotizacion';
            xhr.open('POST', url, true);
            xhr.responseType = 'arraybuffer';
            xhr.onload = function () {
                if (this.status === 200) {
                    var filename = "";
                    var disposition = xhr.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    }
                    var type = xhr.getResponseHeader('Content-Type');

                    var blob = typeof File === 'function'
                    ? new File([this.response], filename, { type: type })
                    : new Blob([this.response], { type: type });
                    if (typeof window.navigator.msSaveBlob !== 'undefined') {

                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) {

                            var a = document.createElement("a");

                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100);
                    }
                }
            };
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.send(form.serialize());

});
    </script>