

<table>
	<tr>
		<td>Proceso:</td>
		<th><?=$Info_Proceso['codigo_cliente'].'-'.$Info_Proceso['proceso']?></th>
	</tr>
	<tr>
		<td>Cliente:</td>
		<th><?=$Info_Proceso['nombre']?></th>
	</tr>
	<tr>
		<td>Producto</td>
		<th><?=$Info_Proceso['nombre_proceso']?></th>
	</tr>
</table>




<form name="revisa-form" id="revisa-form" action="/hojas_revision/nueva_revision/revisar/<?=$Id_Pedido?>" method="post">


	<?php
	foreach ($Items_Revision as $Index => $Item)
	{
		?>
		<br />
		<table style="width: 75%;" class="table table-hover tabular">
			<tr>
				<th style="width: 80%;"><?=$Item['item']?></th>
				<th>Revisado</th>
			</tr>
			<?php
			foreach ($Item['sub_item'] as $SubIndex => $Sub_Item)
			{
				?>
				<tr>
					<td><?=$Sub_Item['sub_item']?></td>
					<td>
						<!-- <select name="item_<?=$SubIndex?>">
							<option value="--">--</option>
							<option value="OK">OK</option>
							<option value="N/A">N/A</option>
						</select> -->
						<div class="checkbox  col-md-offset-4" style="padding: 0px; margin-top: 0px;">
							<input type="checkbox" name="item_<?=$SubIndex?>">
						</div>

					</td>
				</tr>
				<?php
			}
			?>
		</table>
		<?php
	}
	?>


	<br />


	<strong>OBSERVACIONES</strong>
	<br />
	<textarea name="rev_observaciones" cols="85" rows="10"></textarea>

	<br />
	<!-- <input type="button" class="btn btn-link" value="Finalizar Revisi&oacute;n" onclick="validar_hoja()" /> -->
	<button class="btn btn-success" onclick="validar_hoja()">Finalizar Revisi&oacute;n</button>


</form>


<br />




<script>
	
	function validar_hoja(){
		
		var continuar = true;
		$('select').each(function()
		{
			if('--' == $(this).val())
			{
				continuar = false;
			}
		});
		
		
		if(!continuar)
		{
			alert('Aun hay items sin revisar');
			return false;
		}
		
		
		
		if(confirm('La hoja de revision sera guardada.\n\rDesea continuar?'))
		{
			$('#revisa-form').submit();
		}
		
	}
	
</script>


